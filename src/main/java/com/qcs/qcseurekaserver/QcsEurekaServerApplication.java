package com.qcs.qcseurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class QcsEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(QcsEurekaServerApplication.class, args);
	}

}

