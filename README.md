## QCS Eureka Server
- http://localhost:9292/actuator/health
- http://localhost:9292/

## Deploy to Heroku
- mvn clean heroku:deploy 
- https://qcs-eureka-server.herokuapp.com/
- https://qcs-eureka-server.herokuapp.com/actuator/health